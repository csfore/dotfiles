# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/csfore/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

#################################################
#                   Aliases                     #
#################################################
alias vim="nvim"
alias sudo='sudo '
alias cd..="cd .."
alias doas='doas '
alias :q="exit"
alias :w="echo $1"
alias ls="exa --icons"

#################################################
#                   Env Vars                    #
#################################################
export CARGO_TARGET_DIR=/home/csfore/.cargo-artifacts
path+=('/home/csfore/.local/bin')
export XDG_DATA_HOME="/home/csfore/.config"

#################################################
#                  Path Updates                 #
#################################################
path+=('/home/csfore/.local/bin')

#################################################
#                  Random Stuff                 #
#################################################
PS1='[%F{1}%n%f%F{4}@%m%f %F{6}%~%f]$ '
