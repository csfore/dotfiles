# dotfiles

Simple structure: .config is in ~/.config, other directories are their respective paths (/etc, and so on)

I don't own any of the images in static/

Portage config files will be uploaded when I have time

## Specs

### General Info

* Linux Distro - Gentoo
* DE/WM - herbstluftwm
* Terminal - Alacritty
* Browser - FireFox
* Kernel - linux
* Shell - ZSH
* Theme - I'm not sure, I don't use GTK3 apps enough to notice

### System Specs

#### Desktop

* CPU - AMD Ryzen 9 5900X
* GPU - Sapphire Nitro+ Radeon RX 6750 XT 12GB GDDR6
* RAM - 32GB DDR4-3600
* Motherboard - Asus TUF Gaming X570-PLUS
* PSU - Seasonic Focus GX-850
* Storage (Yes, it's a lot)
    * Western Digital Black 4 TB 3.5" 7200RPM Internal Hard Drive
    * Western Digital WD_BLACK SN750 2 TB M.2-2280 NVME Solid State Drive	
    * Samsung 870 Evo 1 TB 2.5" Solid State Drive
    * Western Digital Blue 2 TB 3.5" 7200RPM Internal Hard Drive
    * Samsung 860 Evo 1 TB 2.5" Solid State Drive

#### Laptop

* CPU - AMD Ryzen 9 5900HX
* GPU - Nvidia RTX 3070 Mobile
* RAM - 32GB DDR4 (not sure about frequency)
* Model - HP 15z-en100
* Storage
		* 1TB M.2 NVME
		* 512GB M.2 NVME
